package classes;

import main.Main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Review {

    private Map<String, Integer> genres;
    private ArrayList<String> score_phrases;
    private ArrayList<Double> scores;

    private String bestGame;
    private String worseGame;

    public Review() {
        this.genres = new TreeMap<String, Integer>();
        this.score_phrases = new ArrayList<String>();
        this.scores = new ArrayList<Double>();
    }

    public Review attReview(String[] line) {
        Double score = Double.parseDouble(line[3]);

        if (compareWorse(score))
            setWorseGame(line[0]);
        if (compareBest(score))
            setBestGame(line[0]);

        if (line[1].contains(Main.CONSOLE)) {
            if (!genres.containsKey(line[4]))
                genres.put(line[4], 1);
            else genres.put(line[4], genres.get(line[4]) + 1);
        }

        this.score_phrases.add(line[2]);
        this.scores.add(score);

        Collections.sort(scores);

        return this;
    }

    private boolean compareWorse(Double value) {
        boolean isWorse = false;
        if (this.scores.size() > 0) {
            if (getScores().get(0) >= value)
                isWorse = true;
        } else
            isWorse = true;

        return isWorse;
    }

    private boolean compareBest(Double value) {
        boolean isBest = false;
        if (this.scores.size() > 0) {
            if (getScores().get(getScores().size() - 1) <= value)
                isBest = true;
        } else {
            isBest = true;
        }

        return isBest;
    }

    public Map<String, Integer> getGenres() {
        return genres;
    }

    public void setGenres(Map<String, Integer> genres) {
        this.genres = genres;
    }

    public ArrayList<String> getScore_phrases() {
        return score_phrases;
    }

    public void setScore_phrases(ArrayList<String> score_phrases) {
        this.score_phrases = score_phrases;
    }

    public ArrayList<Double> getScores() {
        return scores;
    }

    public void setScores(ArrayList<Double> scores) {
        this.scores = scores;
    }

    public String getBestGame() {
        return bestGame;
    }

    public void setBestGame(String bestGame) {
        this.bestGame = bestGame;
    }

    public String getWorseGame() {
        return worseGame;
    }

    public void setWorseGame(String worseGame) {
        this.worseGame = worseGame;
    }

    public double getPercentageScore_phases() {
        int count = 0;
        for (String str : getScore_phrases())
            if (str.equalsIgnoreCase(Main.SCORE_PHRASE))
                count++;

        double result = count * 100;
        return result / getScore_phrases().size();
    }

    public double getAverageScores() {
        double media = 0;
        for (double val : getScores())
            media += val;

        return media / getScores().size();
    }

    public double getVariance() {
        double average = getAverageScores();
        double temp = 0;
        for (double a : getScores())
            temp += (a - average) * (a - average);

        return temp / (getScores().size() - 1);
    }

    public double getStdDev() {
        return Math.sqrt(getVariance());
    }
}
