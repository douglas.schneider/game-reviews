package main;

import classes.Review;
import utils.SimpleReader;

import java.util.Map;
import java.util.TreeMap;

/* Desenvolvido por Douglas Schneider (598038)
 *  Tarefa 0 - sumarizar para cada gênero de jogo */

public class Main {

    private static final String ARCHIVE = "/home/doug/Downloads/game-reviews.csv";
    private static final boolean IGNORE_FIRST_LINE = true;
    public static final String SCORE_PHRASE = "Amazing";
    public static final String CONSOLE = "Nintendo";

    public static void main(String[] args) {
        printResults(getMap());
    }

    private static Map<String, Review> getMap() {
        Map<String, Review> map = new TreeMap<String, Review>();

        SimpleReader file = new SimpleReader(ARCHIVE);
        String line = file.readLine();

        if (IGNORE_FIRST_LINE)
            line = file.readLine();

        while (line != null) {
            String[] words = line.split(";");

            if (!map.containsKey(words[4])) {
                map.put(words[4], new Review().attReview(words));
            } else {
                map.put(words[4], map.get(words[4]).attReview(words));
            }

            line = file.readLine();
        }
        file.close();

        return map;
    }

    private static void printResults(Map<String, Review> map) {
        Map<String, Integer> mapConsole = new TreeMap<String, Integer>();

        StringBuilder stringBuilder = new StringBuilder();
        for (String genre : map.keySet()) {
            Review review = map.get(genre);

            stringBuilder.append("-----------------------------------------------------").append("\n");
            stringBuilder.append("Genêro: ").append(genre).append("\n");
            stringBuilder.append("  ").append("Número de reviews: ").append(review.getScores().size()).append("\n");
            stringBuilder.append("  ").append("Percentual de \"Amazing\" reviews: ").append(review.getPercentageScore_phases()).append("\n");
            stringBuilder.append("  ").append("Média aritmética dos scores: ").append(review.getAverageScores()).append("\n");
            stringBuilder.append("  ").append("Desvio padrão populacional dos scores: ").append(review.getStdDev()).append("\n");
            stringBuilder.append("  ").append("Melhor jogo: ").append(review.getBestGame()).append("\n");
            stringBuilder.append("  ").append("Pior jogo: ").append(review.getWorseGame()).append("\n");
            stringBuilder.append("\n");

            for (String gen : review.getGenres().keySet()) {
                if (!mapConsole.containsKey(gen))
                    mapConsole.put(gen, review.getGenres().get(gen));
                else mapConsole.put(gen, mapConsole.get(gen) + review.getGenres().get(gen));
            }
        }

        stringBuilder.append("*******************************************************************").append("\n");
        stringBuilder.append("Qual o gênero de jogo mais comum na família de consoles \"Nintendo\"?").append("\n");
        stringBuilder.append("  ").append(getKeyGenreCommon(mapConsole)).append("\n");
        stringBuilder.append("*******************************************************************");

        System.out.println(stringBuilder.toString());
    }

    private static String getKeyGenreCommon(Map<String, Integer> mapConsole) {
        String genre = "";
        int count = 0;
        for (String word : mapConsole.keySet())
            if (count <= mapConsole.get(word)) {
                genre = word;
                count = mapConsole.get(word);
            }

        return genre;
    }
}
